//
//  ColorViewController.swift
//  SimpleApp
//
//  Created by Jonathan Brodie on 4/21/15.
//  Copyright (c) 2015 Grant Terrien. All rights reserved.
//

import UIKit
class ColorViewController: UIViewController  {

    @IBOutlet weak var myLabel: UILabel!

    @IBOutlet weak var slider1: UISlider!
    @IBOutlet weak var slider2: UISlider!
    @IBOutlet weak var slider3: UISlider!
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBackground()
        updateText()
        myLabel.text="Use the sliders to change the background and me!"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sliderChanged(sender: UISlider) {
        updateBackground()
        updateText()
    }
    func updateBackground() {
        let red = CGFloat(slider1.value)
        let blue = CGFloat(slider2.value)
        let green = CGFloat(slider3.value)
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        view.backgroundColor=color
    }
    func updateText() {
        let red = 1.0 - CGFloat(slider1.value)
        let blue = 1.0 - CGFloat(slider2.value)
        let green = 1.0 - CGFloat(slider3.value)
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        myLabel.textColor=color
    }

}