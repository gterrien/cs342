//
//  ProgressBarViewController.swift
//  SimpleApp
//
//  Created by Grant Terrien on 4/21/15.
//  Copyright (c) 2015 Grant Terrien. All rights reserved.
//

import UIKit

class ProgressBarViewController: UIViewController {

    @IBOutlet weak var testButton: UIButton!
    @IBOutlet weak var progressBarView: UIProgressView!
    var timer: NSTimer!
    
    var progress : Float = 0 {
        didSet{
            if progress <= 1.1 {
                progressBarView.setProgress(progress, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        testButton.setTitle("Testing...", forState: UIControlState.Disabled)
        testButton.setTitle("Test the progress bar", forState: UIControlState.Normal)
        progressBarView.setProgress(0, animated: false)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func testButtonPushed(sender: UIButton) {
        testButton.enabled = false
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
    }
    
    func update() {
        progress = progress + 0.05
        if (progress==1.1) {
            testButton.enabled=true
            progressBarView.setProgress(0,animated:false)
            progress=0
            timer.invalidate()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
