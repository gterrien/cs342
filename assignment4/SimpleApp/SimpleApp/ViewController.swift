//
//  ViewController.swift
//  SimpleApp
//
//  Created by Grant Terrien on 4/12/15.
//  Copyright (c) 2015 Grant Terrien. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let items = ["Web View", "Image with Sound", "Controls", "Progress Bar","Background Color"]
    let cellIdentifier = "cell"
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        tableView?.allowsSelection = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
        }
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        cell.textLabel?.text = self.items[indexPath.row]
        
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.None
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        
        if (cell?.textLabel?.text == "Web View") {
            performSegueWithIdentifier("webViewSegue", sender: nil)
        }
        
        if (cell?.textLabel?.text == "Image with Sound") {
            performSegueWithIdentifier("imageSoundViewSegue", sender: nil)
        }
        
        if (cell?.textLabel?.text == "Controls") {
            performSegueWithIdentifier("controlsViewSegue", sender: nil)
        }
        
        if (cell?.textLabel?.text == "Progress Bar") {
            performSegueWithIdentifier("progressBarViewSegue", sender: nil)
        }
        if (cell?.textLabel?.text == "Background Color") {
            performSegueWithIdentifier("colorviewSegue", sender: nil)
        }
        
    }
    
}






