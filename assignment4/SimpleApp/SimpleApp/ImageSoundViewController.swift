//
//  ImageSoundViewController.swift
//  SimpleApp
//
//  Created by Grant Terrien on 4/19/15.
//  Copyright (c) 2015 Grant Terrien. All rights reserved.
//

import UIKit
import AVFoundation


class ImageSoundViewController: UIViewController {
    

    @IBOutlet weak var imageView: UIImageView!
    
    var player : AVPlayer?
    var item: AVPlayerItem?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        var image = UIImage(named: "dog") // Random dog picture from Reddit
        imageView.image = image
        imageView.contentMode = .ScaleAspectFit
        let url = NSURL(string: "http://www.wavsource.com/snds_2015-04-12_5971820382841326/animals/dog_bark_x.wav")
        item = AVPlayerItem(URL: url)
        player = AVPlayer(playerItem: item)


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        player?.play()
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        player?.pause()
        // Rewind
        let startTime = CMTimeMake(0, 1)
        player?.seekToTime(startTime)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
