//
//  ControlsViewController.swift
//  SimpleApp
//
//  Created by Grant Terrien on 4/19/15.
//  Copyright (c) 2015 Grant Terrien. All rights reserved.
//

import UIKit

class ControlsViewController: UIViewController {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    var numSwitchesOn : Int?
    var numTimesSwipedRight : Int?
    
    
    @IBOutlet var switches: [UISwitch]!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        for aSwitch : UISwitch in switches {
            aSwitch.on = false
        }
        numSwitchesOn = 0
        numTimesSwipedRight = 0
        label1.text = "You have switched on 0 switch(es)"
        label2.text = "You have swiped right 0 time(s)"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func switch1Switched(sender: UISwitch) {
        switchSwitched()
    }
    
    @IBAction func switch2Switched(sender: UISwitch) {
        switchSwitched()
    }
    
    @IBAction func switch3Switched(sender: UISwitch) {
        switchSwitched()
    }
    
    @IBAction func switch4Switched(sender: UISwitch) {
        switchSwitched()
    }
    
    
    func switchSwitched() {
        numSwitchesOn = 0
        for aSwitch: UISwitch in switches {
            if aSwitch.on {
                numSwitchesOn = numSwitchesOn! + 1
            }
        }
        label1.text = "You have switched on \(numSwitchesOn!) switch(es)"
    }
    
    @IBAction func swipedRight(sender: UISwipeGestureRecognizer) {
        numTimesSwipedRight = numTimesSwipedRight! + 1
        label2.text = "You have swiped right \(numTimesSwipedRight!) time(s)"
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
