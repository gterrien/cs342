//
//  WebViewController.swift
//  SimpleApp
//
//  Created by Grant Terrien on 4/12/15.
//  Copyright (c) 2015 Grant Terrien. All rights reserved.
//

import UIKit

class WebViewController: UIViewController  {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var forwardButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var request = NSURLRequest(URL: NSURL(string: "http://carleton.edu")!)
        self.webView.loadRequest(request)
        
        
    }

    @IBAction func handleBackButton(sender: UIButton) {
        if (webView.canGoBack) {
            webView.goBack()
        }
}
    
    @IBAction func handleForwardButton(sender: UIButton) {
        if (webView.canGoForward) {
            webView.goForward()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
